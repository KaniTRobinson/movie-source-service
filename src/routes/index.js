const express = require('express')
const router = express.Router()

router.use('/3388', require('./_3388.routes'))
router.use('/two-embed', require('./two-embed.routes'))
router.use('/box', require('./box.routes'))
router.use('/download', require('./download.routes'))

module.exports = router
