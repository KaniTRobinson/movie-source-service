const express = require('express')
const twoEmbedController = require('../controllers/two-embed.controller')
const handleErrors = require('../utils/handle-errors')

const router = express.Router()

router.get('/:type', handleErrors(twoEmbedController.getByIds))
router.get('/:type/:imdbId', handleErrors(twoEmbedController.getById))
router.get('/:type/:imdbId/:series/:episode', handleErrors(twoEmbedController.getById))

module.exports = router

