const express = require('express')
const boxController = require('../controllers/box.controller')
const handleErrors = require('../utils/handle-errors')

const router = express.Router()

router.get('/redirect', handleErrors(boxController.redirect))
router.get('/authorisation', handleErrors(boxController.authorize))
router.get('/getDirectoryItems', handleErrors(boxController.getDirectoryItems))

module.exports = router
