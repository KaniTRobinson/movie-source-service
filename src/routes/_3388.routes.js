const express = require('express')
const _3388Controller = require('../controllers/3388.controller')
const handleErrors = require('../utils/handle-errors')

const router = express.Router()

router.get('/scrapeMovies', handleErrors(_3388Controller.scrapeMovies))

module.exports = router

