const express = require('express')
const downloadController = require('../controllers/download.controller')
const handleErrors = require('../utils/handle-errors')

const router = express.Router()

router.get('/movies', handleErrors(downloadController.downloadMovies))

module.exports = router
