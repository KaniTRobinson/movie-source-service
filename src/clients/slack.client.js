const axios = require('axios')
const props = require('../props.js')

class SlackClient {
  constructor() {
    this.webhookUrl = `https://hooks.slack.com/services/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`
  }

  sendNotification(blocks = []) {
    return axios.post(this.webhookUrl, {
      blocks: [...blocks, { type: 'context', elements: [{ type: 'plain_text', text: `Server: ${props.port}` }] }],
    })
  }

  success(text) {
    return this.sendNotification([{ type: 'section', text: { type: 'plain_text', text: `✅ ${text}`, emoji: true } }])
  }

  error(text) {
    return this.sendNotification([{ type: 'section', text: { type: 'plain_text', text: `📛 ${text}`, emoji: true } }])
  }

  info(text) {
    return this.sendNotification([{ type: 'section', text: { type: 'plain_text', text: `ℹ️ ${text}`, emoji: true } }])
  }
}

module.exports = new SlackClient()
