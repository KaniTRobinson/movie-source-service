const args = process.argv.slice(2);
const port = args[0] || process.env.PORT || 3001
const tokenString = `token${port}`
const tokenFile = `/root/movie-source-service/src/cache/${tokenString}.json`

module.exports = {
	args,
	port,
	tokenString,
	tokenFile,
}
