module.exports = (type, id, series = null, episode = null) =>
  type === 'tv'
    ? `https://www.2embed.ru/embed/imdb/tv?id=${id}&s=${series}&e=${episode}`
    : `https://www.2embed.ru/embed/imdb/movie?id=${id}`
