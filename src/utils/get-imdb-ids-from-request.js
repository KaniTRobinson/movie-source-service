module.exports = (req) =>
  req.query.imdbIds ? JSON.parse(Buffer.from(req.query.imdbIds, 'base64').toString('ascii')) : []
