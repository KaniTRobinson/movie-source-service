module.exports = {
	domain: 'https://www.3388.to',
	movieList: 'https://www.3388.to/filter?sort=latest&type=movie&quality=all&genre=&country=&release_year=all',
	seriesList: 'https://www.3388.to/filter?sort=latest&type=tv&quality=all&genre=&country=&release_year=all',
	searchList: 'https://www.3388.to/search?q={query}',
}
