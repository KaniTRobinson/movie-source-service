const express = require('express')
const router = require('./routes')
const path = require('path')
const app = express()

app.use(function (req, res, next) {
  req.setTimeout(0)
  next()
})
app.use('/output', express.static(path.join(__dirname, '../output')))
app.use('/api/v1', router)

module.exports = app
