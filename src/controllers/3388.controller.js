const _3388Service = require('../services/3388.service')

class _3388Controller {
	async scrapeMovies(req, res) {
		const series = 'series' === req.query.type
		const data = await _3388Service.scrapeMovies(req, series)
		return res.send({data})
	}
}

module.exports = new _3388Controller()
