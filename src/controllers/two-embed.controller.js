const twoEmbedService = require('../services/two-embed.service')
const getImdbIdsFromRequest = require('../utils/get-imdb-ids-from-request')

class TwoEmbedController {
  async getByIds(req, res) {
    const type = req.params.type
    const imdbIds = getImdbIdsFromRequest(req, type)
    const data = await twoEmbedService.scrapeByIds(req, type, imdbIds)
    return res.send({ data })
  }

  async getById(req, res) {
    const type = req.params.type
    const imdbId = req.params.imdbId
    const series = req.params.series || null
    const episode = req.params.episode || null
    console.log({ type, imdbId, series, episode })
    const data = await twoEmbedService.scrapeById(req, type, imdbId, series, episode)
    return res.send({ data })
  }
}

module.exports = new TwoEmbedController()
