const boxService = require('../services/box.service')

class BoxController {
  async redirect(req, res) {
    const authorizeUrl = await boxService.getAuthorizeUrl()
    return res.redirect(authorizeUrl)
  }

  async authorize(req, res) {
    if (!req.query.code) return res.redirect('/api/v1/box/redirect')
    const token = await boxService
      .getTokensAuthorizationCodeGrant(req.query.code)
      .catch(() => res.redirect('/api/v1/box/redirect'))
    return res.send(token)
  }

  async getDirectoryItems(req, res) {
		const id = req.query.id
  	const content = await boxService.getDirectoryItems(id)
		console.log(content.length)
		return res.send(content)
	}
}

module.exports = new BoxController()
