const axios = require('axios')
const baseService = require('../services/base.service')
const os = require('os')
const fs = require('fs')

class DownloadController
{
	async downloadMovies(request, response) {
		const lockFile = __dirname + '/../../output/movieLockFile'
		if (false === fs.existsSync(lockFile)) {
			fs.writeFileSync(lockFile, '1')
			const movieUrl = 'https://user.gomo.to/user-api/movies?key=b3B1Y25hS2VtSnVVbG5Pc29xT2dsSnlmWVphaW9LK25tS1dVVTZPc29nPT0&p=1'
			const embedGetUrl = 'http://0.0.0.0:3001/api/v1/two-embed/movie/'
			const result = await axios.get(movieUrl)
			const data = result.data
			const service = new baseService()
			for (const key in data) {
				const movie = data[key]
				const uri = embedGetUrl + movie.imdId
				const twoEmbedResponse = await axios.get(uri)
				const twoEmbedData = twoEmbedResponse.data.data
				// if we find such a source.
				console.log(twoEmbedData)
				if (twoEmbedData.id) {
					for (const subtitleKey in twoEmbedData.subtitles) {
						const subtitle = twoEmbedData.subtitles[subtitleKey]
						await service.downloadSubtitles(subtitle.file, movie.imdId, subtitle.label)
					}
					await service.downloadSource(twoEmbedData.source, twoEmbedData.id)
				}
			}
			response.send('Started!')
		} else {
			response.send('Already running!')
		}
	}
}

module.exports = new DownloadController()
