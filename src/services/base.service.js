const fs = require('fs')
const playwright = require('playwright')
const m3u8ToMp4 = require('m3u8-to-mp4')
const boxService = require('./box.service')
const slackClient = require('../clients/slack.client')
const axios = require('axios')

const converter = new m3u8ToMp4()

module.exports = class BaseService {
  /**
   * Create a browser instance.
   * @returns Playwright
   */
  createBrowser() {
	  return playwright.firefox.launch({
		  headless: true,
		  ignoreHTTPSErrors: true,
		  downloadsPath: __dirname + '/../../output/'
	  })
  }

  /**
   * Create a new browser context
   * @param {browser} browser
   * @returns Context
   */
  createContext(browser, options) {
    if (!browser) throw new Error('You cannot create a context without a browser.')
    return browser.newContext(options)
  }

  async getContext(browser, skipAuth = false) {
	  const token = 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6MTQ4MjMxLCJpYXQiOjE2NDY3Njc3MzcsImV4cCI6MTY3ODMwMzczN30.yoNAaYkDOZt3b_KgnJtSjkoMdla9LRAJM0TXRkH_9Ms'
	  const options = (true === skipAuth) ? {
		  userAgent: 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/99.0.4844.51 Safari/537.36',
		  extraHTTPHeaders: {
			  userAgent: 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/99.0.4844.51 Safari/537.36',
		  },
	  } : {
		  // viewport: { width: 1280, height: 1024 },
		  acceptDownloads: true,
		  userAgent: 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/99.0.4844.51 Safari/537.36',
		  authorization: 'Bearer ' + token,
		  extraHTTPHeaders: {
			  userAgent: 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/99.0.4844.51 Safari/537.36',
			  authorization: 'Bearer ' + token,
		  }
	  }
	  const context = await this.createContext(browser, options)
	  await context.addCookies((true === skipAuth) ? [] : [
		  {
			  name: '_accessToken',
			  value: '' + token,
			  path: '/',
			  domain: '.3388.to',
		  },
		  {
			  name: '_profile',
			  value: '163657',
			  path: '/',
			  domain: '.3388.to',
		  }
	  ]);
	  return {
		  context,
		  options,
	  }
  }

	/**
	 * @param context
	 * @param url
	 * @param options
	 * @returns {Promise<*>}
	 */
  async createPage(context, url, options) {
		if (!context) throw new Error('You cannot create a page without a context.')
		const page = await context.newPage(options)
		await page.goto(url, {waitUntil: 'load', timeout: 1000 * 60})
		await page.setDefaultTimeout(1000 * 60)
		await page.setDefaultNavigationTimeout(1000 * 60)
		return page
	}

  /**
   * Download the video source.
   * @param {string} source
   * @param {string} type
   * @param {string} id
   * @param {string|null} series
   * @param {string|null} episode
   * @returns String
   */
  async downloadSource(source, id, childId = undefined) {
    const imdbId = childId || id
    const fileName = `${imdbId}.mp4`
    const fileExists = await boxService.fileExists(fileName)
    const filePath = `output/${fileName}`

    if (!fileExists) {
      await slackClient.info(`Downloading Source ${fileName}`)
      await converter.setInputFile(source).setOutputFile(filePath).start()
      await this.uploadSource(filePath, fileName)
    } else {
    	console.log(`Already got source ${fileName}`)
	}

    return filePath
  }

	/**
	 *
	 * @param source
	 * @param id
	 * @param label
	 * @returns {Promise<string>}
	 */
  async downloadSubtitles(source, id, label) {
    const imdbId = id
    const fileName = `${imdbId}-${label}.vtt`
    const fileExists = await boxService.fileExists(fileName)
    const filePath = `output/${fileName}`

    if (!fileExists) {
		await slackClient.info(`Downloading Subtitles ${fileName}`)
		const response = axios.get(source)
		fs.writeFileSync(filePath, response.data)
		await this.uploadSubtitles(filePath, fileName)
	} else {
    	console.log(`Already got subtitles for ${fileName}`)
	}

    return filePath
  }

  async uploadSubtitles(filePath, fileName) {
    await slackClient.success(`Finished Downloading Subtitles ${fileName}`)
    await boxService.uploadFile(filePath, fileName)
    fs.unlink(filePath, () => {})
  }

  async uploadSource(filePath, fileName, series = false) {
	  await boxService.refreshToken()
	  await slackClient.success(`Finished Downloading Source ${fileName}`)
	  await boxService.uploadFile(filePath, fileName, series)
	  await boxService.refreshToken()
	  fs.unlink(filePath, () => {
	  })
  }

  /**
   * Close a tab.
   * @param {page} page
   * @returns Boolean
   */
  closePage(page) {
    if (!page) throw new Error('You cannot close a page that does not exist.')
    return page.close()
  }

  /**
   * Stops the browser instance.
   * @param {browser} browser
   * @returns Boolean
   */
  closeBrowser(browser) {
    if (!browser) throw new Error('You cannot close a browser that does not exist.')
    return browser.close()
  }
}
