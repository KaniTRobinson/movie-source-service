const BaseService = require('./base.service')
const _3388Uri = require('../utils/3388-uri')
const { selectors } = require('playwright')
const fs = require('fs')
const axios = require('axios')
const slackClient = require('../clients/slack.client')
const boxService = require('./box.service')
const path = require('path')
const pkg = require('follow-redirects')
const  { https } = pkg
const m3u8ToMp4 = require("m3u8-to-mp4")
const converter = new m3u8ToMp4()
const levenshtein = require('js-levenshtein')

class _3388 extends BaseService {

	/**
	 *
	 * @param request
	 * @param series
	 * @returns {Promise<void>}
	 */
  async scrapeMovies(request, series = false) {
		const pageIds = request.query.page ? request.query.page.split(',') : [1]
		const pageItemsOffset = parseInt(request.query.pageItemsOffset) || 0
		const browser = await this.createBrowser()
		const {context, options} = await this.getContext(browser)

		const search = request.query.search || null

		for (const pageIdKey in pageIds) {
			const pageId = pageIds[pageIdKey]
			const uri = (search) ? _3388Uri.searchList.replace('{query}', search) : (((false === series) ? _3388Uri.movieList : _3388Uri.seriesList) + ((pageId > 1) ? '&page=' + pageId : ''))
			const page = await this.createPage(context, uri, options)
			// await page.screenshot({path: 'screenshot.png'})
			await page.waitForSelector('.block_area-content', {timeout: 10000})
			const delay = async (time) => new Promise(resolve => setTimeout(resolve(true), time))

			let items = await page.$$('.film_list-ul li')
			let m3u8Url = null
			let lastM3u8Url = null
			const data = []

			for (const key in items) {
				const item = items[key]
				const link = await item.$('a')
				let title = await item.$('.film-name')
				let year = await item.$('.film-detail .fd-item:nth-child(2)')
				const uri = _3388Uri.domain + await link.getAttribute('href')
				title = await title.innerText()
				year = parseInt(await year.innerText())
				data.push({uri, title, year})
			}

			for (const pageKey in data) {
				if (pageKey < pageItemsOffset)
					continue
				const {uri, title, year} = data[pageKey]
				let filename
				let fileExists

				if (true === series) {
					let { watchPage, optionsButton, pageItems, poster, quality } = await this.getSeriesWatchPage(context, uri, options)
					watchPage.on('request', request => {
						const url = request.url()
						if (-1 !== url.indexOf('.m3u8')) {
							if (-1 !== url.indexOf('/1080/')) {
								m3u8Url = url
								quality = 1080
							} else if (-1 !== url.indexOf('/720/') && quality < 720) {
								m3u8Url = url
								quality = 720
							} else if (-1 !== url.indexOf('/480/') && quality < 480) {
								m3u8Url = url
								quality = 360
							} else if (-1 !== url.indexOf('/360/') && quality < 360) {
								m3u8Url = url
								quality = 360
							}
						}
					})
					const playButton = await watchPage.$('.wicon.wicon-play')

					for (let key in pageItems) {
						const item = pageItems[key]
						const link = await item.$('a')
						let id = await link.innerText()
						id = parseInt(id.replace(/[a-z\s]+/ig, ''))
						console.log('poster click and hover 1')
						await poster.click({ force: true, timeout: 10000 })
						await poster.hover({ force: true, timeout: 10000 })
						console.log('hover 1')
						await optionsButton.hover({ force: true, timeout: 10000})
						await optionsButton.hover({ force: true, timeout: 10000})
						await watchPage.screenshot({path: `screenshot-2-${key}.png`})
						try {
							console.log('linkel click 1')
							await link.click({force: true, timeout: 10000}) // click the season to open the list
						} catch (error) {
							console.log('link el click 1 error')
						}
						const episodes = await watchPage.$$('.cmecb-epslist ul li')
						const imdbData = await this.getImdbId(title, year, id, episodes.length)
						if (null !== imdbData) {
							for (const epKey in episodes) {
								const optionsButton1 = await watchPage.$('.wc-item.wc-eps.wc-item-pop')
								// watchPage.waitForSelector('.wwc-controls', { timeout: 10000 })
								const poster = await watchPage.$('.wwc-controls')
								const row = episodes[epKey]
								const key = parseInt(epKey) + 1
								try {
									console.log('poster click and hover 2')
									await poster.click({force: true, timeout: 10000})
									console.log('hover 2')
									await poster.hover({force: true, timeout: 10000})
								} catch (error) {
									console.log('poster click and hover 2 error')
								}
								await watchPage.screenshot({path: 'screenshot-2.png'})
								try {
									await optionsButton.hover({force: true, timeout: 10000})
									await optionsButton.hover({force: true, timeout: 10000})
								} catch (error) {
									console.log('options button error hover')
								}
								const link = await row.$('a') // play the series
								let linkEl
								if (1 === epKey && 1 === episodes.length) {
									console.log(`.cmecb-epslist ul li:nth-child(0)`)
									linkEl = await watchPage.$(`.cmecb-epslist ul li:nth-child(0)`)
								} else {
									console.log(`.cmecb-epslist ul li:nth-child(${key})`)
									linkEl = await watchPage.$(`.cmecb-epslist ul li:nth-child(${key})`)
								}
								try {
									console.log('linkel click 2')
									await linkEl.click({force: true, timeout: 10000}) // click the episode
								} catch (error) {
									console.log('link el error click')
								}

								const episodeTitle = await link.innerText()
								const episode = this.getEpisodeFromImdbData(epKey, episodeTitle, imdbData)

								console.log('episode', episode)

								const imdbId = ('undefined' !== typeof episode.id) ? episode.id : episode.title
								const filename = `${imdbId}.mp4`
								const fileExists = await boxService.fileExists(filename)

								// await watchPage.screenshot({path: 'screenshot-3.png'})

								if (false === fileExists) {
									// Subscribe to 'request' and 'response' events.

									await slackClient.info(`Starting Downloading ${title} ${imdbId}`)
									await watchPage.waitForSelector('.wc-item.wc-play', {timeout: 10000})

									await new Promise(async resolve => {
										let promise
										setTimeout(() => {
											console.log('resolve')
											// promise.abort()
											resolve(true)
										}, 3000)
										try {
											console.log('play click')
											promise = await Promise.all([
												watchPage.waitForResponse('*/playlist.m3u8'),
												watchPage.click('.wc-item.wc-play'),
												watchPage.click('[aria-labelledby="menu-quality"] > div div:nth-child(3)'),
											])
										} catch (error) {
										}
									})

									console.log('downloading', m3u8Url)

									// while (null === m3u8Url) {
									// 	await delay(100)
									// }


									// stop playing it!
									// try {
									// 	console.log('play button click')
									// 	await playButton.click({force: true, timeout: 10000})
									// 	// await playButton.click({ force: true, timeout: 10000 })
									// } catch (error) {
									// 	console.log('play button click error')
									// }

									// await this.closePage(watchPage)
									// await delay(1000)
									// watchPage = null

									if (null !== m3u8Url) {

										if (true === await this.isValidSource(m3u8Url)) {
											const newPath = __dirname + '/../../output/' + filename
											await converter
												.setInputFile(m3u8Url)
												.setOutputFile(newPath)
												.start()

											await this.uploadSource(newPath, filename, true)
										} else {
											console.log(`Couldn't find suitable source [404] ${filename}`)
											await slackClient.info(`Couldn't find suitable source [404] ${filename}`)
										}
									} else {
										console.log(`Couldn't find suitable source ${filename}`)
										await slackClient.info(`Couldn't find suitable source ${filename}`)
									}
								} else {
									if (true === fileExists) {
										console.log(`We already have ${title}`)
									} else {
										console.log(`Couldn't find the imdb title ${title}`)
									}
								}
								lastM3u8Url = m3u8Url
								m3u8Url = null
								quality = 0
							}
						}
					}
					this.closePage(watchPage)
				}
				else if (false === series) {
					const imdbId = await this.getImdbId(title, year)
					if (null !== imdbId) {
						filename = imdbId + '.mp4'

						fileExists = await boxService.fileExists(filename)

						if (false === fileExists) {
							const watchPage = await this.createPage(context, uri, options)
							// Subscribe to 'request' and 'response' events.
							let quality = 0
							watchPage.on('request', request => {
								const url = request.url()
								if (-1 !== url.indexOf('.m3u8')) {
									if (-1 !== url.indexOf('/1080/')) {
										m3u8Url = url
										quality = 1080
									} else if (-1 !== url.indexOf('/720/') && quality < 720) {
										m3u8Url = url
										quality = 720
									} else if (-1 !== url.indexOf('/480/') && quality < 480) {
										m3u8Url = url
										quality = 360
									} else if (-1 !== url.indexOf('/360/') && quality < 360) {
										m3u8Url = url
										quality = 360
									}
								}
							})

							await slackClient.info(`Starting Downloading ${title} ${imdbId}`)
							await watchPage.waitForSelector('.wicon.wicon-play', {timeout: 10000})

							await new Promise(async resolve => {
								let promise
								setTimeout(() => {
									console.log('resolve')
									// promise.abort()
									resolve(true)
								}, 3000)
								try {
									promise = await Promise.all([
										watchPage.waitForResponse('*/playlist.m3u8'),
										watchPage.click('.wicon.wicon-play'),
										watchPage.click('[aria-labelledby="menu-quality"] > div div:nth-child(3)'),
									])
								} catch (error) {
								}
							})

							console.log('downloading', m3u8Url)

							// while (null === m3u8Url) {
							// 	await delay(100)
							// }

							await this.closePage(watchPage)

							if (null !== m3u8Url) {

								if (true === await this.isValidSource(m3u8Url)) {
									const newPath = __dirname + '/../../output/' + filename
									await converter
										.setInputFile(m3u8Url)
										.setOutputFile(newPath)
										.start()

									await this.uploadSource(newPath, filename)
								} else {
									console.log(`Couldn't find suitable source [404] ${filename}`)
									await slackClient.info(`Couldn't find suitable source [404] ${filename}`)
								}
							} else {
								console.log(`Couldn't find suitable source ${filename}`)
								await slackClient.info(`Couldn't find suitable source ${filename}`)
							}
						} else {
							if (true === fileExists) {
								console.log(`We already have ${title}`)
							} else {
								console.log(`Couldn't find the imdb title ${title}`)
							}
						}
					}
					lastM3u8Url = m3u8Url
					m3u8Url = null
				}
			}
			this.closePage(page)
		}
		await this.closeBrowser(browser)
		process.exit()
	}

	async getSeriesWatchPage(context, uri, options) {
		const watchPage = await this.createPage(context, uri, options)
		let quality = 0

		// await watchPage.screenshot({path: 'screenshot1.png'})
		await watchPage.waitForSelector('.wc-right', { timeout: 8000 })
		const pageItems = await watchPage.$$('.cmecb-seasons ul li')

		const optionsButton = await watchPage.$('.wc-item.wc-eps.wc-item-pop')
		watchPage.waitForSelector('.wwc-controls', { timeout: 10000 })
		const poster = await watchPage.$('.wwc-controls')
		console.log('poster click')
		await poster.click({ force: true, timeout: 10000 })
		await poster.hover({ force: true, timeout: 10000 })
		// await optionsButton.hover({ force: true, timeout: 10000})
		// await optionsButton.hover({ force: true, timeout: 10000})
		return {
			watchPage,
			quality,
			pageItems,
			optionsButton,
			poster,
		}
	}


	getEpisodeFromImdbData(index, title, imdbData) {
		return imdbData.episodes[index]
	}

	async isValidSource(source) {
		const status = await new Promise(async resolve => {
			const request = await https.get(source, (res) => {
				resolve(res.statusCode)
			})
		})
		return (-1 === [404].indexOf(status))
	}

	async getImdbId(title, year, series = null, episodeTotal = null) {
		title = title.replace(/\s/g, '_').replace(/[^a-z0-9.()_]+/gi, '').toLowerCase()
		const uri = 'https://v2.sg.media-imdb.com/suggestion/{first}/{query}.json'.replace('{query}', title).replace('{first}', title.substring(0, 1))
		const delay = async (time) => new Promise(resolve => setTimeout(resolve(true), time))
		// console.log(uri)
  		const response = await axios.get(uri)
		let id = null
		// no data!
		if (response.data.d) {
			const result = response.data.d.filter(result => {
				const a = result.l.replace(/[^a-z0-9]/gi, '').toLowerCase()
				const b = title.replace(/[^a-z0-9]/gi, '').toLowerCase()
				return (a === b || levenshtein(a, b) <= 2) && result.y === year
			})
			id = result.length ? result[0].id : null
		}
		if (null !== series && null !== id) {
			await delay(3000)
			const episodeListURI = `https://www.imdb.com/title/${id}/episodes?season=${series}`
			// console.log(episodeListURI)
			const browser = await this.createBrowser()
			const {context, options} = await this.getContext(browser, true)
			let statusCode
			context.on('response', (response) => statusCode = response.status())
			const page = await this.createPage(context, episodeListURI, options)
			// console.log(statusCode)
			if (404 !== statusCode) {
				await page.waitForSelector('.list.detail.eplist .list_item', {timeout: 10000})
				const items = await page.$$('.list.detail.eplist .list_item')
				const data = []
				for (const key in items) {
					const item = items[key]
					const metadata = await item.$('[itemprop="episodeNumber"]')
					const id = await item.$('.wtw-option-standalone')
					const title = await item.$('strong')
					data.push({
						episode: parseInt(await metadata.getAttribute('content')),
						id: await id.getAttribute('data-tconst'),
						title: await title.innerText(),
					})
				}
				if (data.length < episodeTotal) {
					for (let i = data.length; i < episodeTotal; i++) {
						data.push({
							title: id + '-episode-' + (i + 1),
							episode: (i + 1),
						})
					}
				}
				this.closePage(page)
				this.closeBrowser(browser)
				return {
					id,
					episodes: data,
				}
			} else {
				// console.log('MADE', episodeTotal, Array(episodeTotal).fill(0, 0, episodeTotal).map((value, key) => 'Episode ' + ++key))
				this.closePage(page)
				this.closeBrowser(browser)
				return {
					id,
					episodes: Array(episodeTotal).fill(0, 0, episodeTotal).map(
						(value, key) => {
							return {
								title: id + '-episode-' + ++key,
								episode: key,
							}
						}
					),
				}
			}
		}
		return id
	}
}

module.exports = new _3388()
