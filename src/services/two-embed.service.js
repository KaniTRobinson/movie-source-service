const BaseService = require('./base.service')
const getTwoEmbedUri = require('../utils/get-two-embed-uri')

class ImdbService extends BaseService {
  /**
   * Scraping movie sources by multiple imdbIds
   * @param {string} type
   * @param {array} data
   * @returns Array
   */
  async scrapeByIds(req, type, data) {
    const items = []
    const browser = await this.createBrowser()
    for (const imdb of data) {
      const { id, series = undefined, episode = undefined, childId = undefined } = imdb
      if (childId) req.query.childId = childId
      const item = await this.scrapeById(req, type, id, series, episode, browser)
      if (item) items.push(item)
    }
    await this.closeBrowser(browser)
    return items
  }

  /**
   * Scraping movie source by imdbId.
   * @param {string} type
   * @param {string} id
   * @param {integer|null} series
   * @param {integer|null} episode
   * @param {browser|null} parentBrowser
   * @returns Object
   */
  async scrapeById(req, type, id, series = undefined, episode = undefined, parentBrowser = null) {
    const childId = req.query.childId || undefined
    const browser = !parentBrowser ? await this.createBrowser() : parentBrowser
    const context = await this.createContext(browser)
    const page = await this.createPage(context, getTwoEmbedUri(type, id, series, episode))
    const title = await page.title()
    if (title == '404 Page Not Found') return {}
    await page.waitForSelector('.fa-play', { timeout: 5000 })
    await page.dispatchEvent('.fa-play', 'click')
    await page.waitForSelector('#iframe-embed[src]', { timeout: 5000 })
    const iframe = await page.$('#iframe-embed')
    const frame = await iframe.contentFrame()
    await frame.waitForSelector('.jw-video')
	  console.log('here')
    const { source, subtitles } = await frame.evaluate(() => {
		return {
			source: sources,
			subtitles: tracks.filter(({kind}) => kind === 'captions'),
		}
	})
    await this.closePage(page)
    if (!parentBrowser) await this.closeBrowser(browser)

    // await this.downloadSource(source, id, childId)

    return { id, source, type, series, episode, childId, subtitles }
  }
}

module.exports = new ImdbService()
