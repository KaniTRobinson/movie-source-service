const fs = require('fs')
const path = require('path')
const BoxSDK = require('box-node-sdk')
const slackClient = require('../clients/slack.client')
const props = require('../props')
const tokenFile = props.tokenFile

if (false === fs.existsSync(tokenFile)) {
	console.log(tokenFile)
	fs.writeFileSync(tokenFile, '[]')
}

const tokenData = require(tokenFile)

let token = tokenData

class BoxService {
  constructor() {
    const { BOX_SERIES_CONTENT_DIRECTORY, BOX_CLIENT_ID, BOX_CLIENT_SECRET, BOX_CONTENT_DIRECTORY, BOX_SUBTITLE_DIRECTORY } = process.env
    this.box = new BoxSDK({ clientID: BOX_CLIENT_ID, clientSecret: BOX_CLIENT_SECRET })
    this.contentDirectoryId = BOX_CONTENT_DIRECTORY
    this.seriesContentID = BOX_SERIES_CONTENT_DIRECTORY
    this.subtitleDirectoryId = BOX_SUBTITLE_DIRECTORY
    this.token = null
    this.client = null
  }

  async getDirectoryItems(id) {
		await this.getPersistentClient()
		const items = []
		let last
		let options = {
			offset: 0
		}
		do {
			last = await this.client.folders.get(id, options)
			last = last.item_collection.entries
			items.push(...last)
			options.offset += 100
		} while (last.length > 0)
		return items
	}

  async getAuthorizeUrl() {
    return this.box.getAuthorizeURL({ response_type: 'code' })
  }

  async getTokensAuthorizationCodeGrant(code) {
    const authorizationToken = await this.box.getTokensAuthorizationCodeGrant(code)
    await fs.writeFileSync(path.resolve(__dirname, tokenFile), JSON.stringify(authorizationToken))
    return authorizationToken
  }

  async refreshToken() {
	  if (!token.refreshToken) return false
	  const authorizationToken = await this.box.getTokensRefreshGrant(token.refreshToken)
	  token = authorizationToken
	  await fs.writeFileSync(path.resolve(__dirname, tokenFile), JSON.stringify(authorizationToken))
	  this.getPersistentClient(authorizationToken)
	  return this.client
  }

  async getPersistentClient(authorizationToken = null) {
    if (!token.refreshToken) return false
    this.client = await this.box.getPersistentClient(authorizationToken || token)
    return this.client
  }

  async checkAuthorized() {
    const client = await this.getPersistentClient()
    if (!client) await this.refreshToken()
    if (!this.client) {
      slackClient.error('Please re-authenticate to box on the video uploading service')
      return false
    }
    return this.client
  }

  async getFile(fileName) {
    try {
      await this.checkAuthorized()
      const { entries } = await this.client.search.query(fileName)
      return entries.find(({ name }) => name === fileName)
    } catch (err) {
      slackClient.error('Please re-authenticate to box on the video uploading service')
      return true
    }
  }

  async fileExists(fileName) {
	  await this.refreshToken()
	  const exists = await this.getFile(fileName)
	  return !!exists
  }

  async uploadFile(filePath, fileName, series = false) {
    try {
      slackClient.info(`Beginning uploading ${fileName}`)
      await this.checkAuthorized()
      const stream = fs.createReadStream(filePath)
      const contentLength = fs.statSync(filePath).size
      const { entries } = await this.client.files.uploadFile(((true === series) ? this.seriesContentID : this.contentDirectoryId), fileName, stream, {
        content_length: contentLength,
      })
      slackClient.success(`Finished uploading ${fileName}`)
      return entries
    } catch (error) {
      console.error(error)
      slackClient.error(`There was an issue uploading ${fileName}`)
      return false
    }
  }
}

module.exports = new BoxService()
