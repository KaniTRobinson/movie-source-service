require('dotenv').config()
const props = require('./props.js')
const app = require('./app')

app.listen(props.port, () => console.log(`Express service listening on port ${props.port}`))
